import { HomeAlmoxarifadoComponent } from './almoxarifado/home-almoxarifado/home-almoxarifado.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PaginaNaoEncontradaComponent } from './core/pagina-nao-encontrada/pagina-nao-encontrada.component';


const routes: Routes = [
  { path: '', component: HomeAlmoxarifadoComponent },
  { path: 'almoxarifado', component: HomeAlmoxarifadoComponent },
  { path: 'pagina-nao-encontrada', component: PaginaNaoEncontradaComponent },
  { path: '**', component: PaginaNaoEncontradaComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
