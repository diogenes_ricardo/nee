import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ErrorHandlerService } from './error-handler.service';
import { ConfirmationService } from 'primeng/api';
import { Title } from '@angular/platform-browser';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { GrowlModule } from 'primeng/growl';

import { UtilsComponent } from './utils/utils.component';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    ConfirmDialogModule,
    GrowlModule

  ],
  declarations: [
    PaginaNaoEncontradaComponent,
    UtilsComponent
  ],
  exports: [
    PaginaNaoEncontradaComponent,
    UtilsComponent
  ],
  providers: [
    ErrorHandlerService,
    ConfirmationService,
    Title,

    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ]
})
export class CoreModule { }
