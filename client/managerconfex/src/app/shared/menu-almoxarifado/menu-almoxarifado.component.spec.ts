import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuAlmoxarifadoComponent } from './menu-almoxarifado.component';

describe('MenuAlmoxarifadoComponent', () => {
  let component: MenuAlmoxarifadoComponent;
  let fixture: ComponentFixture<MenuAlmoxarifadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuAlmoxarifadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuAlmoxarifadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
