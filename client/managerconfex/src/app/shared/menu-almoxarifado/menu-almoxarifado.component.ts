import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-menu-almoxarifado',
  templateUrl: './menu-almoxarifado.component.html',
  styleUrls: ['./menu-almoxarifado.component.css']
})
export class MenuAlmoxarifadoComponent implements OnInit {

  items: MenuItem[];

  constructor() { }

  ngOnInit() {

    this.items = [
      {
          label: 'Cadastros',
          items: [{
                  label: 'Classificaçoes',
                  routerLink: ['/almoxarifado/classificacoes']
                  /* icon: 'fa fa-fw fa-plus', */
                  /* items: [
                      {label: 'Project'},
                      {label: 'Other'},
                  ] */
              },
              {
                label: 'Cores',
                routerLink: ['almoxarifado/cores']
              },
              {
                label: 'Fornecedores',
                routerLink: ['almoxarifado/fornecedores']
              },
              {
                label: 'Insumos',
                routerLink: ['almoxarifado/insumos']
              },
          ]
      },
      {
          label: 'Processos',
          /* icon: 'fa fa-fw fa-edit', */
          items: [
              {label: 'Movimentações Estoque'/* , icon: 'fa fa-fw fa-mail-forward' */}
          ]
      }
  ];
  }

}
