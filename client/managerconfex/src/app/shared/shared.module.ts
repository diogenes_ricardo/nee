import { MenuAlmoxarifadoComponent } from './menu-almoxarifado/menu-almoxarifado.component';
import { AlmoxarifadoModule } from '../almoxarifado/almoxarifado.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabViewModule } from 'primeng/tabview';
import { MenubarModule } from 'primeng/menubar';

import { MenuModulosComponent } from './menu-modulos/menu-modulos.component';
import { RouterModule } from '@angular/router';
import { MessageAlertComponent } from './message-alert/message-alert.component';

@NgModule({
  imports: [
    CommonModule,
    TabViewModule,
    MenubarModule,
    AlmoxarifadoModule,

    RouterModule

  ],
  declarations: [
    MenuModulosComponent,
    MenuAlmoxarifadoComponent,
    MessageAlertComponent
  ],
  exports: [
    MenuModulosComponent,
    MessageAlertComponent
  ]
})
export class SharedModule { }
