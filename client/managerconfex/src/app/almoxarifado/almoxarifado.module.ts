import { ClassificacoesService } from './cadastros/classificacoes/classificacoes.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ButtonModule } from 'primeng/button';
import { ChartModule } from 'primeng/chart';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';

import { HomeAlmoxarifadoComponent } from './home-almoxarifado/home-almoxarifado.component';
import { FornecedoresComponent } from './cadastros/fornecedores/fornecedores.component';
import { CoresComponent } from './cadastros/cores/cores.component';
import { ClassificacoesComponent } from './cadastros/classificacoes/classificacoes.component';
import { InsumosComponent } from './cadastros/insumos/insumos.component';

import { AlmoxarifadoRoutingModule } from './almoxarifado-routing.module';
import { PainelCoresComponent } from './cadastros/cores/painel-cores/painel-cores.component';

import { CoresService } from './cadastros/cores/cores.service';
import { HttpClientModule } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { PainelClassificacoesComponent } from './cadastros/classificacoes/painel-classificacoes/painel-classificacoes.component';
@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    FormsModule,
    HttpClientModule,

    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    BrowserAnimationsModule,

    AlmoxarifadoRoutingModule

  ],
  declarations: [
    HomeAlmoxarifadoComponent,
    ClassificacoesComponent,
    CoresComponent,
    FornecedoresComponent,
    InsumosComponent,
    PainelCoresComponent,
    PainelClassificacoesComponent
  ],
  providers: [
    CoresService,
    ClassificacoesService,
    MessageService
   ],
  exports: [HomeAlmoxarifadoComponent]
})
export class AlmoxarifadoModule { }
