import { PainelClassificacoesComponent } from './cadastros/classificacoes/painel-classificacoes/painel-classificacoes.component';
import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';

import { ClassificacoesComponent } from './cadastros/classificacoes/classificacoes.component';
import { FornecedoresComponent } from './cadastros/fornecedores/fornecedores.component';
import { InsumosComponent } from './cadastros/insumos/insumos.component';
import { PainelCoresComponent } from './cadastros/cores/painel-cores/painel-cores.component';
import { CoresComponent } from './cadastros/cores/cores.component';

const routes: Routes = [
  /* { path: '', component: HomeAlmoxarifadoComponent }, */
  { path: 'almoxarifado/classificacoes', component: PainelClassificacoesComponent },
  { path: 'almoxarifado/classificacoes/:codigo', component: ClassificacoesComponent },
  { path: 'almoxarifado/classificacoes/nova', component: ClassificacoesComponent },

  { path: 'almoxarifado/cores', component: PainelCoresComponent },
  { path: 'almoxarifado/cores/:codigo', component: CoresComponent },
  { path: 'almoxarifado/cores/nova', component: CoresComponent },

  { path: 'almoxarifado/fornecedores', component: FornecedoresComponent },
  { path: 'almoxarifado/insumos', component: InsumosComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AlmoxarifadoRoutingModule { }
