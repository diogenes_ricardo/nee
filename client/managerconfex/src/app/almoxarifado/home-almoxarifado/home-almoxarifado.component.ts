import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';



@Component({
  selector: 'app-home-almoxarifado',
  templateUrl: './home-almoxarifado.component.html',
  styleUrls: ['./home-almoxarifado.component.css']
})
export class HomeAlmoxarifadoComponent implements OnInit {

  data: any;

  dataLines: any;

  msgs: Message[];

  constructor() {
    this.data = {
      labels: ['Tecidos', 'Entretelas', 'Filamentos'],
      datasets: [
        {
          data: [300, 50, 100],
          backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ],
          hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ]
        }]
    };

    this.dataLines = {
      labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho'],
      datasets: [
          {
              label: 'Média da Produtividade Geral',
              data: [65, 59, 80, 81, 56, 55, 40],
              fill: false,
              borderColor: '#4bc0c0'
          },
          {
              label: 'Capacidade Operacional Diária',
              data: [28, 48, 40, 19, 86, 27, 90],
              fill: false,
              borderColor: '#565656'
          }
      ]
  };
  }

  ngOnInit() {
  }

  selectData(event) {
    this.msgs = [];
    // tslint:disable-next-line:max-line-length
    this.msgs.push({severity: 'info', summary: 'Data Selected', 'detail': this.dataLines.datasets[event.element._datasetIndex].data[event.element._index]});
}

}
