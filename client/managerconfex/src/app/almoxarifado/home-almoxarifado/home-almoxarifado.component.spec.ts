import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAlmoxarifadoComponent } from './home-almoxarifado.component';

describe('HomeAlmoxarifadoComponent', () => {
  let component: HomeAlmoxarifadoComponent;
  let fixture: ComponentFixture<HomeAlmoxarifadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAlmoxarifadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAlmoxarifadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
