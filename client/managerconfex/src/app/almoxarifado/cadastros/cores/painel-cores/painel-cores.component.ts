import { Component, Input } from '@angular/core';
import { CoresService, CorFiltro } from '../cores.service';
import { LazyLoadEvent, ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { ErrorHandlerService } from '../../../../core/error-handler.service';

@Component({
  selector: 'app-painel-cores',
  templateUrl: './painel-cores.component.html',
  styleUrls: ['./painel-cores.component.css']
})
export class PainelCoresComponent {

  @Input() cores: Array<any>;
  @Input() filtro = new CorFiltro();
  totalRegistros = 0;

  constructor(
    private corService: CoresService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private errorHanler: ErrorHandlerService
  ) { }

  proximaPagina(event: LazyLoadEvent) {
    this.filtro.pagina = event.first / event.rows;
    this.corService.consultar(this.filtro).subscribe(
      response => {
        this.cores = response['content'];
        this.totalRegistros = response['totalElements'];
      }
    );
  }

  confirmarExclusao(lancamento: any) {
    this.confirmationService.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
        this.excluir(lancamento);
      }
    });
  }

  excluir(Cor: any) {
    this.corService.excluir(Cor.codigo).subscribe(
      response => {
        this.listarcores(this.filtro.pagina);
        this.messageService.add({ severity: 'info', summary: 'Atualização', detail: 'Cor excluída com sucesso' });
      }, error => this.errorHanler.handler(error)
    );
  }

  listarcores(pagina = 0) {
    this.filtro.pagina = pagina;
    this.corService.consultar(this.filtro).subscribe(
      response => {
        this.cores = response['content'];
        this.totalRegistros = response['totalElements'];
      }, error => this.errorHanler.handler(error)
    );
  }

  ativarCor(cor: any) {
    this.corService.ativar(cor).subscribe(
      response => {
        this.listarcores(this.filtro.pagina);
        let status = 'ativada';
        if (cor.ativo === true) {
          status = 'desativada';
          this.messageService.add({ severity: 'info', summary: 'Desativação', detail: 'Cor ' + status });
        } else {
          this.messageService.add({ severity: 'info', summary: 'Ativação', detail: 'Cor ' + status });
        }

      }, error => this.errorHanler.handler(error)
    );
  }

  listarCor(pagina = 0) {
    this.filtro.pagina = pagina;
    this.corService.consultar(this.filtro).subscribe(
      response => {
        this.cores = response['content'];
        this.totalRegistros = response['totalElements'];
      }, error => this.errorHanler.handler(error)
    );
  }

}
