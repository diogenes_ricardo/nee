import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Cor } from '../../../core/model/Cor';


export class CorFiltro {
  descricao: string;
  pagina = 0;
  itensPorPagina = 5;
}

@Injectable()
export class CoresService {

  coresURL = 'http://localhost:8080/cores';
  parameters:  HttpParams;

  constructor(private http: HttpClient) { }

  consultar(filtro: CorFiltro) {
    this.parameters = new HttpParams();
    if (filtro.descricao) { // todo: PROBLEMA DO UNDEFINED
      this.parameters = this.parameters.set('nome', filtro.descricao);
    }
    this.parameters = this.parameters.set('page', filtro.pagina.toString());
    this.parameters = this.parameters.set('size', filtro.itensPorPagina.toString());
    this.parameters.set('descricao', filtro.descricao);

    const httpOptions = {
    /*   headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      }), */
      params: this.parameters
    };

    return this.http.get<Array<any>>(`${this.coresURL}` , httpOptions);
  }

  excluir(codigo: number) {
/*     const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      }),
      params: this.parameters
    }; */
    return this.http.delete(`${this.coresURL}/${codigo}`/* , httpOptions */);
  }

  ativar(cor: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        /* 'Authorization': 'Basic YWRtaW46YWRtaW4=', */
        'Content-Type': 'application/json'
      })
    };
    let ativo = true;
    if (cor.ativo === true) {
      ativo = false;
    }
    return this.http.put(`${this.coresURL}/${cor.codigo}/ativo`, ativo , httpOptions);
  }

  listar() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      })
    };
    return this.http.get<Array<any>>(`${this.coresURL}`, httpOptions);
  }

  listarCor(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      })
    };
    return this.http.get<Cor>(`${this.coresURL}/${id}`, httpOptions);
  }

  adicionar(cor: Cor): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4=',
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<Cor>(this.coresURL, cor, httpOptions);
  }

}
