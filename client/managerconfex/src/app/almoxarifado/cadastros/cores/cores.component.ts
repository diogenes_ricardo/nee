import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

import { ErrorHandlerService } from '../../../core/error-handler.service';
import { MessageService } from 'primeng/api';
import { Cor } from '../../../core/model/Cor';

import { CoresService } from './cores.service';

@Component({
  selector: 'app-cores',
  templateUrl: './cores.component.html',
  styleUrls: ['./cores.component.css']
})
export class CoresComponent implements OnInit {

  cor = new Cor();

  constructor(
    private corService: CoresService,
    private errorHanler: ErrorHandlerService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('Cadastro de Cores');
    console.log(this.route.snapshot.params['codigo']);
    if (this.route.snapshot.params['codigo'] !== 'nova') {
      this.title.setTitle('Editar cor');
      const corID = this.route.snapshot.params['codigo'];
      this.carregarCor(corID);
    }
  }

  get isEditando() {
    return Boolean(this.cor.codigo);
  }

  salvar(form: FormControl) {
    this.corService.adicionar(this.cor).subscribe(
      response => {
        if (this.isEditando) {
          this.messageService.add({ severity: 'info', summary: 'Sucesso', detail: 'Cor alterada com sucesso' });
        } else {
          this.messageService.add({ severity: 'info', summary: 'Sucesso', detail: 'Cor cadastrada com sucesso' });
        }
        form.reset(); // resetando o formulario
        this.cor = new Cor(); // limpando o objeto
        this.router.navigate(['/almoxarifado/cores']);
      }, error => this.errorHanler.handler(error)
    );
  }

  carregarCor(id: number) {
    this.corService.listarCor(id).subscribe(
      response => {
        this.cor = response;
      }, error => this.errorHanler.handler(error)
    );
  }
}
