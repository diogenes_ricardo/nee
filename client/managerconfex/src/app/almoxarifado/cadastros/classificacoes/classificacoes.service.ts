import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Classificacao } from '../../../core/model/Classificacao';


export class ClassificacaoFiltro {
  descricao: string;
  pagina = 0;
  itensPorPagina = 5;
}

@Injectable()
export class ClassificacoesService {

  classificacoesURL = 'http://localhost:8080/classificacoes';
  parameters:  HttpParams;

  constructor(private http: HttpClient) { }

  consultar(filtro: ClassificacaoFiltro) {
    this.parameters = new HttpParams();
    if (filtro.descricao) { // todo: PROBLEMA DO UNDEFINED
      this.parameters = this.parameters.set('nome', filtro.descricao);
    }
    this.parameters = this.parameters.set('page', filtro.pagina.toString());
    this.parameters = this.parameters.set('size', filtro.itensPorPagina.toString());
    this.parameters.set('descricao', filtro.descricao);

    const httpOptions = {
    /*   headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      }), */
      params: this.parameters
    };

    return this.http.get<Array<any>>(`${this.classificacoesURL}` , httpOptions);
  }

  excluir(codigo: number) {
/*     const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      }),
      params: this.parameters
    }; */
    return this.http.delete(`${this.classificacoesURL}/${codigo}`/* , httpOptions */);
  }

  ativar(classificacao: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        /* 'Authorization': 'Basic YWRtaW46YWRtaW4=', */
        'Content-Type': 'application/json'
      })
    };
    let ativo = true;
    if (classificacao.ativo === true) {
      ativo = false;
    }
    return this.http.put(`${this.classificacoesURL}/${classificacao.codigo}/ativo`, ativo , httpOptions);
  }

  listar() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      })
    };
    return this.http.get<Array<any>>(`${this.classificacoesURL}`, httpOptions);
  }

  listarClassificacao(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4='
      })
    };
    return this.http.get<Classificacao>(`${this.classificacoesURL}/${id}`, httpOptions);
  }

  adicionar(classificacao: Classificacao): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Basic YWRtaW46YWRtaW4=',
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<Classificacao>(this.classificacoesURL, classificacao, httpOptions);
  }

}
