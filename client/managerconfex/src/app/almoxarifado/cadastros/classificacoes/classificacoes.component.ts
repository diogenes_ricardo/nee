import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';

import { MessageService } from 'primeng/api';
import { ErrorHandlerService } from '../../../core/error-handler.service';
import { Classificacao } from '../../../core/model/Classificacao';
import { ClassificacoesService } from './classificacoes.service';

@Component({
  selector: 'app-classificacoes',
  templateUrl: './classificacoes.component.html',
  styleUrls: ['./classificacoes.component.css']
})
export class ClassificacoesComponent implements OnInit {

  classificacao = new Classificacao();

  constructor(
    private classificacaoService: ClassificacoesService,
    private errorHanler: ErrorHandlerService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('Cadastro de Classificações');
    if (this.route.snapshot.params['codigo'] !== 'nova') {
      this.title.setTitle('Editar classificacão');
      const classificacaoID = this.route.snapshot.params['codigo'];
      this.carregarClassificacao(classificacaoID);
    }
  }

  get isEditando() {
    return Boolean(this.classificacao.codigo);
  }

  salvar(form: FormControl) {
    this.classificacaoService.adicionar(this.classificacao).subscribe(
      response => {
        if (this.isEditando) {
          this.messageService.add({ severity: 'info', summary: 'Sucesso', detail: 'Classificacao alterada com sucesso' });
        } else {
          this.messageService.add({ severity: 'info', summary: 'Sucesso', detail: 'Classificacao cadastrada com sucesso' });
        }
        form.reset(); // resetando o formulario
        this.classificacao = new Classificacao(); // limpando o objeto
        this.router.navigate(['/almoxarifado/classificacoes']);
      }, error => this.errorHanler.handler(error)
    );
  }

  carregarClassificacao(id: number) {
    this.classificacaoService.listarClassificacao(id).subscribe(
      response => {
        this.classificacao = response;
      }, error => this.errorHanler.handler(error)
    );
  }
}
