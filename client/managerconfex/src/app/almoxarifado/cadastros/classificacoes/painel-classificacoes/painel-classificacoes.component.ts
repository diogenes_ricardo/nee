import { Component, OnInit, Input } from '@angular/core';
import { ClassificacaoFiltro, ClassificacoesService } from '../classificacoes.service';
import { ConfirmationService, MessageService, LazyLoadEvent } from '../../../../../../node_modules/primeng/api';
import { ErrorHandlerService } from '../../../../core/error-handler.service';

@Component({
  selector: 'app-painel-classificacoes',
  templateUrl: './painel-classificacoes.component.html',
  styleUrls: ['./painel-classificacoes.component.css']
})
export class PainelClassificacoesComponent {

  @Input() classificacoes: Array<any>;
  @Input() filtro = new ClassificacaoFiltro();
  totalRegistros = 0;

  constructor(
    private classificacaoService: ClassificacoesService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private errorHanler: ErrorHandlerService
  ) { }

  proximaPagina(event: LazyLoadEvent) {
    this.filtro.pagina = event.first / event.rows;
    this.classificacaoService.consultar(this.filtro).subscribe(
      response => {
        this.classificacoes = response['content'];
        this.totalRegistros = response['totalElements'];
      }
    );
  }

  confirmarExclusao(lancamento: any) {
    this.confirmationService.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
        this.excluir(lancamento);
      }
    });
  }

  excluir(Classificacao: any) {
    this.classificacaoService.excluir(Classificacao.codigo).subscribe(
      response => {
        this.listarclassificacoeses(this.filtro.pagina);
        this.messageService.add({ severity: 'info', summary: 'Atualização', detail: 'Classificação excluída com sucesso' });
      }, error => this.errorHanler.handler(error)
    );
  }

  listarclassificacoeses(pagina = 0) {
    this.filtro.pagina = pagina;
    this.classificacaoService.consultar(this.filtro).subscribe(
      response => {
        this.classificacoes = response['content'];
        this.totalRegistros = response['totalElements'];
      }, error => this.errorHanler.handler(error)
    );
  }

  ativarClassificacao(classificacao: any) {
    this.classificacaoService.ativar(classificacao).subscribe(
      response => {
        this.listarclassificacoeses(this.filtro.pagina);
        let status = 'ativada';
        if (classificacao.ativo === true) {
          status = 'desativada';
          this.messageService.add({ severity: 'info', summary: 'Desativação', detail: 'Classificação ' + status });
        } else {
          this.messageService.add({ severity: 'info', summary: 'Ativação', detail: 'Classificação ' + status });
        }

      }, error => this.errorHanler.handler(error)
    );
  }

  listarClassificacao(pagina = 0) {
    this.filtro.pagina = pagina;
    this.classificacaoService.consultar(this.filtro).subscribe(
      response => {
        this.classificacoes = response['content'];
        this.totalRegistros = response['totalElements'];
      }, error => this.errorHanler.handler(error)
    );
  }

}
