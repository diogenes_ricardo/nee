import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelClassificacoesComponent } from './painel-classificacoes.component';

describe('PainelClassificacoesComponent', () => {
  let component: PainelClassificacoesComponent;
  let fixture: ComponentFixture<PainelClassificacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PainelClassificacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelClassificacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
