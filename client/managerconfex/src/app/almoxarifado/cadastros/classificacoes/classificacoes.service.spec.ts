import { TestBed, inject } from '@angular/core/testing';

import { ClassificacoesService } from './classificacoes.service';

describe('ClassificacoesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClassificacoesService]
    });
  });

  it('should be created', inject([ClassificacoesService], (service: ClassificacoesService) => {
    expect(service).toBeTruthy();
  }));
});
